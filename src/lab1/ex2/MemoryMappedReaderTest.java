package lab1.ex2;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;

/**
 * Created by adam on 09.04.17.
 */
public class MemoryMappedReaderTest {

    public static void readFile(String fileName) {
        long startTime = System.currentTimeMillis();
        try {
            FileChannel fileChannel = new RandomAccessFile(fileName, "r").getChannel();
            MappedByteBuffer buffer = fileChannel.map(FileChannel.MapMode.READ_ONLY, 0, fileChannel.size());

            for (int i = 0; i < buffer.limit(); i++) {
                buffer.get();
            }

            System.out.println("Memory mapped file read time: " + (System.currentTimeMillis() - startTime));
        } catch (FileNotFoundException e) {
            System.out.println("File not found");
        } catch (IOException e) {
            System.out.println("sth wrong");
        }
    }
}
