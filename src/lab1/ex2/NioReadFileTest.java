package lab1.ex2;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;

/**
 * Created by adam on 09.04.17.
 */
public class NioReadFileTest {

    public static void readFile(String fileName) {
        long startTime = System.currentTimeMillis();
        try {
            RandomAccessFile file = new RandomAccessFile(fileName, "r");

            FileChannel inChannel = file.getChannel();
            long fileSize = inChannel.size();
            ByteBuffer buffer = ByteBuffer.allocate((int) fileSize);
            inChannel.read(buffer);
            buffer.flip();
            for (int i = 0; i < fileSize; i++) {
                buffer.get();
            }
            inChannel.close();
            file.close();

            System.out.println("NIO read File time: " + (System.currentTimeMillis() - startTime));
        } catch (FileNotFoundException e) {
            System.out.println("File not found");
        } catch (IOException e) {
            System.out.println("Sth went wrong :(");
        }
    }
}
