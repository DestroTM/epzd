package lab1.ex2;

import java.io.FileNotFoundException;
import java.io.UnsupportedEncodingException;

public class Ex2Main {

    public static String FILE_NAME = "test.txt";

    public static void main(String[] args) {
        try {
            TextFileGenerator.genrateFile(FILE_NAME);
            BufferedReaderTest.readFile(FILE_NAME);
            NioReadFileTest.readFile(FILE_NAME);
            MemoryMappedReaderTest.readFile(FILE_NAME);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }
}
