package lab1.ex2;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

/**
 * Created by adam on 09.04.17.
 */
public class BufferedReaderTest {

    public static void readFile(String fileName) {
        long startTime = System.currentTimeMillis();
        try {
            FileReader fileReader = new FileReader(fileName);

            BufferedReader bufferedReader = new BufferedReader(fileReader);
            String textLine = bufferedReader.readLine();

            while (textLine != null) {
                textLine = bufferedReader.readLine();
            }

            bufferedReader.close();

        } catch (FileNotFoundException e) {
            System.out.println("File not found");
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("\nBufferedReader Time: " + (System.currentTimeMillis() - startTime));
    }
}
