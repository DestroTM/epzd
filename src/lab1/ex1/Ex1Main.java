package lab1.ex1;

import java.util.Date;

/**
 * Created by adam on 09.04.17.
 */
public class Ex1Main {
    public static void main(String args[]) {
        lab1.ex1.Person person = new lab1.ex1.Person("adam", "goralczyk", new Date(), 19);
        lab1.ex1.Serializator.saveFile(person);
	    lab1.ex1.Serializator.readFileSerializable();

        lab1.ex1.PersonExternalizable personExternalizable = new lab1.ex1.PersonExternalizable("adam", "goralczyk", new Date(), 19);
        lab1.ex1.Serializator.saveFile(personExternalizable);
        lab1.ex1.Serializator.readFileExternalizable();
    }
}
