package lab1.ex1;

import java.io.*;
import java.nio.file.Files;

/**
 * Created by adam on 12.03.17.
 */
public class Serializator {

    public static String FILE_NAME = "file";

    public static void saveFile(Object object) {
        long startTime = System.currentTimeMillis();
        System.out.println("lab1.ex1.Serializator|saveFile|START");
        try {
            OutputStream file = new FileOutputStream(FILE_NAME);
            OutputStream buffer = new BufferedOutputStream(file);
            ObjectOutput output = new ObjectOutputStream(buffer);

            output.writeObject(object);

            output.close();
            buffer.close();

            file.close();

            System.out.println("Size: " + Files.size(new File(FILE_NAME).toPath()));
        }
        catch (IOException ex) {
            ex.printStackTrace();
        }
        System.out.println("lab1.ex1.Serializator|saveFile|STOP|TIME: " + (System.currentTimeMillis() - startTime));
    }

    public static void readFileSerializable() {
        System.out.println("lab1.ex1.Serializator|readFileSerializable|START");
        try {
            InputStream file = new FileInputStream(FILE_NAME);
            InputStream buffer = new BufferedInputStream(file);
            ObjectInput input = new ObjectInputStream(buffer);

            Person person = (Person)input.readObject();
            System.out.println(person.toString());

            input.close();
            buffer.close();
            file.close();
        }
        catch(IOException ex) {
            ex.printStackTrace();
        }
        catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        System.out.println("lab1.ex1.Serializator|readFileSerializable|STOP");
    }

    public static void readFileExternalizable() {
        System.out.println("lab1.ex1.Serializator|readFileExternalizable|START");
        try {
            InputStream file = new FileInputStream(FILE_NAME);
            InputStream buffer = new BufferedInputStream(file);
            ObjectInput input = new ObjectInputStream(buffer);

            PersonExternalizable person = (PersonExternalizable) input.readObject();
            System.out.println(person.toString());

            input.close();
            buffer.close();
            file.close();
        }
        catch (IOException ex) {
            ex.printStackTrace();
        }
        catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        System.out.println("lab1.ex1.Serializator|readFileSerializable|STOP");
    }
}
