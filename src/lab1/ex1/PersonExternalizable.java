package lab1.ex1;

import java.io.*;
import java.util.Date;

/**
 * Created by adam on 12.03.17.
 */
public class PersonExternalizable implements Externalizable {
    private String firstName;
    private String lastName;
    private Date birthday;
    private Integer age;

    public PersonExternalizable() {
    }

    public PersonExternalizable(String firstName, String lastName, Date birthday, Integer age) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.birthday = birthday;
        this.age = age;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    @Override
    public String toString() {
        return "Person{" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", birthday=" + birthday +
                ", age=" + age +
                '}';
    }

    @Override
    public void writeExternal(ObjectOutput out) throws IOException {
        out.writeObject(firstName);
        out.writeObject(lastName);
        out.writeObject(birthday);
        out.writeInt(age);
    }

    @Override
    public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException {
        System.out.println("read External Starts");
        firstName = (String) in.readObject();
        lastName = (String) in.readObject();
        birthday = (Date) in.readObject();
        age = in.readInt();
        System.out.println("read External Ends");
    }
}
