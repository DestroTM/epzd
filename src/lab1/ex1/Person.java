package lab1.ex1;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by adam on 12.03.17.
 */
public class Person implements Serializable {
    private String firstName;
    private String lastName;
    private Date birthday;
    private Integer age;



    public Person(String firstName, String lastName, Date birthday, Integer age) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.birthday = birthday;
        this.age = age;
    }

    @Override
    public String toString() {
        return "lab1.ex1.Person{" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", birthday=" + birthday +
                ", age=" + age +
                '}';
    }
}
