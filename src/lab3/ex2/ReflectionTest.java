package lab3.ex2;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * Created by adam on 13.05.17.
 */
public class ReflectionTest {
    public static void main(String args[]) throws Exception {

        Test test = new Test();

        // Calling class method using reflections
        Method testMethod = Test.class.getMethod("test");
        testMethod.invoke(test);

        // Accessing to public field using reflections
        Field publicField = Test.class.getDeclaredField("publicField");
        System.out.println("Public field value: " + publicField.get(test));

        // Accessing to private field using reflections
        Field privateField = Test.class.getDeclaredField("privateField");
        privateField.setAccessible(true);
        System.out.println("Private field value: " + privateField.get(test));
        privateField.set(test, "beka");
        System.out.println("Chenged private field value: " + privateField.get(test));
    }
}
