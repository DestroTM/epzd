package lab3.ex2;

/**
 * Created by adam on 13.05.17.
 */
public interface Testable {
    void test();
}
