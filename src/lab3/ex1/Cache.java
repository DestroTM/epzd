package lab3.ex1;

/**
 * Created by adam on 14.05.17.
 */
public interface Cache {
    Object get(Object key);
    void put(Object key);
    void fillCache();
    void clearCache();
}
