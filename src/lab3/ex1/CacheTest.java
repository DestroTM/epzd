package lab3.ex1;

/**
 * Created by adam on 13.05.17.
 */
public class CacheTest {
    public static void main(String args[]) {

        final Integer[] FIRST_CAPACITY = {3, 100, 1000};

        for (int i = 0; i < FIRST_CAPACITY.length; i++) {
            // CREATE NEW CACHE
            LinkedHashCache linkedHashCache = new LinkedHashCache(FIRST_CAPACITY[i]);
            HashCache hashCache = new HashCache(FIRST_CAPACITY[i]);
            TreeCache treeCache = new TreeCache(FIRST_CAPACITY[i]);

            System.out.println("CAPACITY " + FIRST_CAPACITY[i]);
            System.out.println("First put: ");
            System.out.println("LinkedHashCache " +  CachePerformanceTest.put(linkedHashCache, "tst") + "ns");
            System.out.println("HashCache " +  CachePerformanceTest.put(hashCache, "tst") + "ns");
            System.out.println("TreeCache " +  CachePerformanceTest.put(treeCache, "tst") + "ns");

            System.out.println("\nGet time: ");
            System.out.println("LinkedHashCache " +  CachePerformanceTest.get(linkedHashCache, "tst") + "ns");
            System.out.println("HashCache " +  CachePerformanceTest.get(hashCache, "tst") + "ns");
            System.out.println("TreeCache " +  CachePerformanceTest.get(treeCache, "tst") + "ns");

            System.out.println("\nPut to full cache: ");
            System.out.println("LinkedHashCache " +  CachePerformanceTest.putWhenCacheIsFull(linkedHashCache, "tst") + "ns");
            System.out.println("HashCache " +  CachePerformanceTest.putWhenCacheIsFull(hashCache, "tst") + "ns");
            System.out.println("TreeCache " +  CachePerformanceTest.putWhenCacheIsFull(treeCache, "tst") + "ns");

            System.out.println("\n\n\n");
        }
    }
}
