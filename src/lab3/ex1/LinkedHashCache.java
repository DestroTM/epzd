package lab3.ex1;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created by adam on 13.05.17.
 */
public class LinkedHashCache extends LinkedHashMap implements Cache {

    private final Integer maxEntries;

    private int age = 1;

    public LinkedHashCache(Integer maxEntries) {
        this.maxEntries = maxEntries;
    }

    @Override
    protected boolean removeEldestEntry(Map.Entry eldest) {
        return size() > maxEntries;
    }

    @Override
    public Object get(Object key) {
        return super.get(key);
    }

    @Override
    public void put(Object key) {
        super.put(key, age++);
    }

    @Override
    public void fillCache() {
        for (int i = this.size(); i < maxEntries; i++) {
            this.put("f" + i, age++);
        }
    }

    @Override
    public void clearCache() {
        this.clear();
    }
}
