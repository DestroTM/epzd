package lab3.ex1;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.TreeMap;

/**
 * Created by adam on 13.05.17.
 */
public class TreeCache implements Cache {

    private final int capacity;

    public TreeCache(int capacity) {
        this.capacity = capacity;
    }

    Map<String, Integer> map = new TreeMap<>();
    int age = 1;

    public void put(String element) {
        if (this.capacity > map.size()) {
            age++;
            map.put(element, age);
        }
        else {
            removeOldestEntry();
            age++;
            map.put(element, age);
        }
    }

    private void removeOldestEntry() {
        Iterator it = map.entrySet().iterator();
        int age = -1;
        String oldestKey = "";
        while (it.hasNext()) {
            Map.Entry pair = (Map.Entry)it.next();
            if (age == -1) {
                age = (int) pair.getValue();
                oldestKey = (String) pair.getKey();
            }
            if ((int) pair.getValue() < age) {
                age = (int) pair.getValue();
                oldestKey = (String) pair.getKey();
            }
        }
        if (age > 0) {
            map.remove(oldestKey);
        }
    }

    public Object get(String key) {
        Iterator it = map.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry pair = (Map.Entry)it.next();
            if (pair.getKey().equals(key)) {
                return pair.getValue();
            }
        }
        return null;
    }

    @Override
    public Object get(Object key) {
        return get((String) key);
    }

    @Override
    public void put(Object key) {
        map.put((String)key, age++);
    }

    @Override
    public void fillCache() {
        for (int i = map.size(); i < capacity; i++) {
            map.put("f" + i, age++);
        }
    }

    @Override
    public void clearCache() {
        map.clear();
    }
}
