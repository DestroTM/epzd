package lab3.ex1;

import java.util.Iterator;
import java.util.List;

/**
 * Created by adam on 09.04.17.
 */
public class CachePerformanceTest {

    public static long put(Cache cache, String key) {
        long startTime = System.nanoTime();
        cache.put(key);
        return System.nanoTime() - startTime;
    }

    public static long get(Cache cache, String key) {
        long startTime = System.nanoTime();
        cache.get(key);
        return System.nanoTime() - startTime;
    }

    public static long putWhenCacheIsFull(Cache cache, String key) {
        cache.fillCache();
        long startTime = System.nanoTime();
        cache.put(key);
        return System.nanoTime() - startTime;
    }
}
