package lab2.ex1;

import java.util.Set;

/**
 * Created by adam on 09.04.17.
 */
public class SetPerformanceTest {

    public static Set generateSetTest(Set<String> set, int size) {
        set.clear();
        for (int i = 0; i < size; i++) {
            set.add(""  + i);
        }
        return set;
    }

    public static long addElementTest(Set<String> set, String element) {
        long startTime = System.nanoTime();
        set.add(element);
        return System.nanoTime() - startTime;
    }

    public static long removeElementTest(Set<String> set, String element) {
        long startTime = System.nanoTime();
        set.remove(element);
        return System.nanoTime() - startTime;
    }

    public static long browseElement(Set<String> set, String element) {
        long startTime = System.nanoTime();
        for (String str : set) {

        }
        return System.nanoTime() - startTime;
    }

    public static long checkIfElementExist(Set<String> set, String element) {
        long startTime = System.nanoTime();
        set.contains(element);
        return System.nanoTime() - startTime;
    }
}
