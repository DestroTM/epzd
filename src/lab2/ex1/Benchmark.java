package lab2.ex1;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.*;

/**
 * Created by adam on 09.04.17.
 */
public class Benchmark {

    public static void main(String args[]) {
        File file = new File("lab2ex1.html");
        try {
            file.createNewFile();
            FileWriter writer = new FileWriter(file);
            writer.write(generateHeader());
            writer.write("HashSet<br>");
            benchmark(writer, new HashSet<String>(10));
            writer.write("LinkedHashSet<br>");
            benchmark(writer, new LinkedHashSet<String>(10));
            writer.write("TreeSet<br>");
            benchmark(writer, new TreeSet<String>());
            writer.write(generateFooter());
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void benchmark(FileWriter writer, Set set) throws IOException {
        writer.write("<table class=\"table table-hover\"><thead><tr><th>Size</th><th>Add</th><th>Remove</th><th>Browse</th><th>Check</th></tr></thead>");
        for (int i = 1; i < 5; i++) {
            int size = (int) Math.pow(10, i);
            set = SetPerformanceTest.generateSetTest(set, size);
            writer.write("<tr>" +
                    "<td>" + size + "</td>" +
                    "<td>" + Long.toString(SetPerformanceTest.addElementTest(set, "TEST")) + "</td>" +
                    "<td>" + Long.toString(SetPerformanceTest.removeElementTest(set, "TEST")) + "</td>" +
                    "<td>" + Long.toString(SetPerformanceTest.browseElement(set, "TEST")) + "</td>" +
                    "<td>" + Long.toString(SetPerformanceTest.checkIfElementExist(set, "TEST")) + "</td>" +
                    "</tr>");
        }

        writer.write("</table>\n");
    }

    private static String generateHeader() {
        String header = "<html><head>" +
                "  <link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css\">\n" +
                "  <script src=\"https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js\"></script>\n" +
                "  <script src=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js\"></script>" +
                "</head><body>";
        return header;
    }

    private static String generateFooter() {
        String footer = "</body></html>";
        return footer;
    }
}
