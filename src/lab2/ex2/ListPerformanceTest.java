package lab2.ex2;

import java.util.Iterator;
import java.util.List;
import java.util.Set;

/**
 * Created by adam on 09.04.17.
 */
public class ListPerformanceTest {

    public static List generateSetTest(List<String> list, int size) {
        list.clear();
        for (int i = 0; i < size; i++) {
            list.add(""  + i);
        }
        return list;
    }

    public static long addElementAtBeginningTest(List<String> list, String element) {
        long startTime = System.nanoTime();
        list.add(0, element);
        return System.nanoTime() - startTime;
    }

    public static long addElementAtEndTest(List<String> list, String element) {
        long startTime = System.nanoTime();
        list.add(list.size() -1, element);
        return System.nanoTime() - startTime;
    }

    public static long addElementAtMidTest(List<String> list, String element) {
        long startTime = System.nanoTime();
        list.add(list.size() / 2, element);
        return System.nanoTime() - startTime;
    }

    public static long removeElementAtBeginningTest(List<String> list) {
        long startTime = System.nanoTime();
        list.remove(0);
        return System.nanoTime() - startTime;
    }

    public static long removeElementAtEndTest(List<String> list) {
        long startTime = System.nanoTime();
        list.remove(list.size() - 1);
        return System.nanoTime() - startTime;
    }

    public static long removeElementAtMidTest(List<String> list) {
        long startTime = System.nanoTime();
        list.remove(list.size() / 2);
        return System.nanoTime() - startTime;
    }

    public static long browseElementUsingIndexes(List<String> list) {
        long startTime = System.nanoTime();
        for (String str : list) {

        }
        return System.nanoTime() - startTime;
    }

    public static long browseElementUsingIterator(List<String> list) {
        long startTime = System.nanoTime();
        Iterator<String> iterator = list.iterator();
        while (iterator.hasNext()) {
            iterator.next();
        }
        return System.nanoTime() - startTime;
    }
}
