package lab2.ex2;

import java.util.Iterator;
import java.util.Queue;

/**
 * Created by adam on 09.04.17.
 */
public class QueuePerformanceTest {

    public static Queue generateSetTest(Queue<String> queue, int size) {
        queue.clear();
        for (int i = 0; i < size; i++) {
            queue.add(""  + i);
        }
        return queue;
    }

    public static long addElementAtBeginningTest(Queue<String> queue, String element) {
        long startTime = System.nanoTime();
        //queue.add("-");
        return 0;
    }

    public static long addElementAtEndTest(Queue<String> queue, String element) {
        long startTime = System.nanoTime();
        queue.add(element);
        return System.nanoTime() - startTime;
    }

    public static long addElementAtMidTest(Queue<String> queue, String element) {
        long startTime = System.nanoTime();
        //queue.add(queue.size() / 2, element);
        return 0;
    }

    public static long removeElementAtBeginningTest(Queue<String> queue) {
        long startTime = System.nanoTime();
        queue.remove(0);
        return System.nanoTime() - startTime;
    }

    public static long removeElementAtEndTest(Queue<String> queue) {
        long startTime = System.nanoTime();
        queue.remove(queue.size() - 1);
        return System.nanoTime() - startTime;
    }

    public static long removeElementAtMidTest(Queue<String> queue) {
        long startTime = System.nanoTime();
        queue.remove(queue.size() / 2);
        return System.nanoTime() - startTime;
    }

    public static long browseElementUsingIndexes(Queue<String> queue) {
        long startTime = System.nanoTime();
        for (String str : queue) {

        }
        return System.nanoTime() - startTime;
    }

    public static long browseElementUsingIterator(Queue<String> queue) {
        long startTime = System.nanoTime();
        Iterator<String> iterator = queue.iterator();
        while (iterator.hasNext()) {
            iterator.next();
        }
        return System.nanoTime() - startTime;
    }
}
