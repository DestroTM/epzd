package lab2.ex2;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.*;

/**
 * Created by adam on 09.04.17.
 */
public class Benchmark {

    public static void main(String args[]) {
        File file = new File("lab2ex2.html");
        try {
            file.createNewFile();
            FileWriter writer = new FileWriter(file);
            writer.write(generateHeader());
            writer.write("ArrayList<br>");
            listBenchmark(writer, new ArrayList<String>(10));
            writer.write("LinkedList<br>");
            listBenchmark(writer, new LinkedList<String>());
            writer.write("Vector<br>");
            listBenchmark(writer, new Vector<String>(10));
            writer.write(generateFooter());
            writer.write("Queue: LinkedList<br>");
            queueBenchmark(writer, new LinkedList<String>());
            writer.write(generateFooter());
            writer.write("Queue: PriorityQueue<br>");
            queueBenchmark(writer, new PriorityQueue<String>(10));
            writer.write(generateFooter());
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void listBenchmark(FileWriter writer, List list) throws IOException {
        writer.write("<table class=\"table table-hover\"><thead><tr><th>Size</th><th>AddBegin</th><th>AddEnd</th><th>AddMid</th><th>RemBeg</th><th>RemEnd</th><th>RemMid</th><th>BrowsInd</th><th>BrowIter</th></tr></thead>");
        for (int i = 1; i < 5; i++) {
            int size = (int) Math.pow(10, i);
            list = ListPerformanceTest.generateSetTest(list, size);
            writer.write("<tr>" +
                    "<td>" + size + "</td>" +
                    "<td>" + Long.toString(ListPerformanceTest.addElementAtBeginningTest(list, "TEST")) + "</td>" +
                    "<td>" + Long.toString(ListPerformanceTest.addElementAtEndTest(list, "TEST")) + "</td>" +
                    "<td>" + Long.toString(ListPerformanceTest.addElementAtMidTest(list, "TEST")) + "</td>" +
                    "<td>" + Long.toString(ListPerformanceTest.removeElementAtBeginningTest(list)) + "</td>" +
                    "<td>" + Long.toString(ListPerformanceTest.removeElementAtEndTest(list)) + "</td>" +
                    "<td>" + Long.toString(ListPerformanceTest.removeElementAtMidTest(list)) + "</td>" +
                    "<td>" + Long.toString(ListPerformanceTest.browseElementUsingIndexes(list)) + "</td>" +
                    "<td>" + Long.toString(ListPerformanceTest.browseElementUsingIterator(list)) + "</td>" +
                    "</tr>");
        }

        writer.write("</table>\n");
    }

    private static void queueBenchmark(FileWriter writer, Queue list) throws IOException {
        writer.write("<table class=\"table table-hover\"><thead><tr><th>Size</th><th>AddBegin</th><th>AddEnd</th><th>AddMid</th><th>RemBeg</th><th>RemEnd</th><th>RemMid</th><th>BrowsInd</th><th>BrowIter</th></tr></thead>");
        for (int i = 1; i < 5; i++) {
            int size = (int) Math.pow(10, i);
            list = QueuePerformanceTest.generateSetTest(list, size);
            writer.write("<tr>" +
                    "<td>" + size + "</td>" +
                    "<td>" + Long.toString(QueuePerformanceTest.addElementAtBeginningTest(list, "TEST")) + "</td>" +
                    "<td>" + Long.toString(QueuePerformanceTest.addElementAtEndTest(list, "TEST")) + "</td>" +
                    "<td>" + Long.toString(QueuePerformanceTest.addElementAtMidTest(list, "TEST")) + "</td>" +
                    "<td>" + Long.toString(QueuePerformanceTest.removeElementAtBeginningTest(list)) + "</td>" +
                    "<td>" + Long.toString(QueuePerformanceTest.removeElementAtEndTest(list)) + "</td>" +
                    "<td>" + Long.toString(QueuePerformanceTest.removeElementAtMidTest(list)) + "</td>" +
                    "<td>" + Long.toString(QueuePerformanceTest.browseElementUsingIndexes(list)) + "</td>" +
                    "<td>" + Long.toString(QueuePerformanceTest.browseElementUsingIterator(list)) + "</td>" +
                    "</tr>");
        }

        writer.write("</table>\n");
    }

    private static String generateHeader() {
        String header = "<html><head>" +
                "  <link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css\">\n" +
                "  <script src=\"https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js\"></script>\n" +
                "  <script src=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js\"></script>" +
                "</head><body>";
        return header;
    }

    private static String generateFooter() {
        String footer = "</body></html>";
        return footer;
    }
}
