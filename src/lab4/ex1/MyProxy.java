package lab4.ex1;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

/**
 * Created by adam on 14.05.17.
 */
public class MyProxy implements InvocationHandler {

    private String methodName;  //method to invoke
    private Object target;      //object to use

    public static Object makeProxy(String methodName, Object target, Class impl) {
        MyProxy myProxy = new MyProxy.Builder(methodName, target).build();
        ClassLoader loader = target.getClass().getClassLoader();

        // create a new proxy, which will implement the interface impl.
        // the object will forward all calls to the proxy through invoke method in myProxy
        return Proxy.newProxyInstance(loader, new Class[]{impl}, myProxy);
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        try {
            Object[] noArgs = {};
            Class[] argTypes = {};
            // we are ignoring arguments

            Method targetMethod = target.getClass().getMethod(methodName, argTypes);
            return targetMethod.invoke(target, noArgs);
        }
        catch (Exception ex) {
            return null;
        }
    }

    // Zrobiłem sobie buildera, bo nigdy nie robiłem i dla zabawy skleciłem
    public static class Builder {
        private final String methodName;
        private final Object target;

        public Builder(final String methodName, final Object target) {
            this.methodName = methodName;
            this.target = target;
        }

        public MyProxy build() {
            return new MyProxy(this);
        }
    }

    private MyProxy(Builder builder) {
        methodName = builder.methodName;
        target = builder.target;
    }


}
