package lab4.ex1;

import lab3.ex2.Testable;

/**
 * Created by adam on 14.05.17.
 */
public class ProxyTest {
    public static void main(String args[]) {
        Testable proxy1 = (Testable) MyProxy.makeProxy("test", new Test1(), Testable.class);
        proxy1.test();

        Testable proxy2 = (Testable) MyProxy.makeProxy("test", new Test2(), Testable.class);
        proxy2.test();
    }
}
